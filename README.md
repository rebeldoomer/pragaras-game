# Pragaras - A Cybernetic Demon Adventure

"Pragaras" is a text-based adventure game set in a dystopian cybernetic underworld. As the player navigates through the digital hellscape, they encounter demons, make allies, and face moral dilemmas. With a rich backstory and dynamic player interactions, "Pragaras" offers a unique experience that combines elements of classic role-playing games with a cyberpunk twist.

## Getting Started

This section will guide you through setting up "Pragaras" on your local machine for development, testing, and gameplay.

### Prerequisites

- Python 3.x
- Tkinter (should be included with Python)
- PIL (Python Imaging Library) for image processing

### Installation

1. Ensure Python is installed on your system. You can download it from [python.org](https://www.python.org/downloads/).

2. Install PIL if it's not already installed:

`pip install Pillow`


3. Clone the repository to your local machine:

`git clone https://gitlab.com/rebeldoomer/pragaras.git`




4. Navigate to the cloned directory:

`cd pragaras`




5. Launch the game:

`python game.py`




## Gameplay

Upon starting "Pragaras," you'll be prompted to enter your name and year of birth. These details influence certain aspects of the game, including dialogue and player references. The game tracks real-time play duration, enhancing immersion and allowing players to reflect on their journey through this digital hellscape.

### Features

- **Dynamic Storyline**: Your choices directly influence the plot's outcome, leading to multiple endings.
- **Immersive Visuals**: Though primarily text-based, "Pragaras" includes vivid imagery to enhance player immersion.
- **Player Interaction**: Engage with NPCs, make critical decisions, and shape your destiny in the cybernetic underworld.
- **Customization**: Player information (name and birth year) affects in-game dialogue and interactions, personalizing the experience.

## Future Developments

- Integration of chiptune background music for an immersive audio experience.
- Expansion of the world-building elements through a player-accessible journal.
- Implementation of combat mechanics, possibly inspired by D&D dice rolls, adding a layer of strategy and chance.
- Introduction of a health point (HP) system and items like health potions for added gameplay depth.

## Contributing

This is a personal project, so I am not looking for any contributions unless it is to offer existing bug fixes. However feel free to study the code and use it as a template by rewriting the entire plot, change graphic visuals, gameplay, etc.

## License

"Pragaras" is shared under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. For more details, visit [CC BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/).

## Acknowledgments

Special thanks to all connections of mine who helped test it so far. Your enthusiasm and feedback drive the continuous improvement and expansion of this dark, cybernetic universe.

## Status
Very early development, in essence it is still unplayable. It will soon get polished up.

